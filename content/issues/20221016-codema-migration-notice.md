---
title: "Codema Migration Notice"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2022-10-16 20:00:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2022-10-17 00:00:00
# You can use: down, disrupted, notice
severity: disrupted
# affected sections (array). Uncomment the affected one's
affected:
# - matrix
# - xmpp
# - diaspora
# - gitlab
# - website
# - mailing-lists
  - loomio
# - videos
# - planet
# - jitsi

# Don't change the value below
section: issue
---
Codema service is being migrated to a new server. Expecting a downtime of around
2 hours.

UPDATE [10 PM, 16th]: There'll be more downtime extending upto 3 hours.

UPDATE [12 AM, 17th]: Migration couldn't be completed as it needs more time than
anticipated. Service is still up and running on the old server now.

UPDATE [2 PM, 17th]: Successfully migrated from Scaleway to Hetzner.
Loomio (codema.in) and Jitsi (meet.fsci.in) are now running on the same server.
