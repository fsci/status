---
title: "Poddery Upgrade Notice"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2024-10-12 11:00:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2024-10-16 00:00:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
 - matrix
 - xmpp
 - diaspora
# - gitlab
# - website
# - mailing-lists
# - loomio
# - videos
# - planet
# - jitsi

# Don't change the value below
section: issue
---
Saturday 12/10/2024 11:00 IST is scheduled for upgrading Poddery server. The process may take up to 6 hours and you may experience some downtime for poddery services during this period.

Update: The upgrade is still going on, it is expected to see intermittent downtime for poddery services on 12/10/24 and 13/10/24.

Update2: We are now on Debian 12, Bookworm. All services are successfully running.
