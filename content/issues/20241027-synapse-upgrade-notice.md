---
title: "Synapse Upgrade Notice"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2024-10-27 20:00:00
# Status of the issue. Boolean value: true, false
resolved: false
# resolved date. format: yyyy-mm-dd hh:mm:ss
# resolvedWhen: 2024-11-01 16:00:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
 - matrix
# - xmpp
# - diaspora
# - gitlab
# - website
# - mailing-lists
# - loomio
# - videos
# - planet
# - jitsi

# Don't change the value below
section: issue
---
Sunday 27/10/2024 08:00 PM IST is scheduled for upgrading synapse. The process may take up to 6 hours and you may experience some downtime for matrix service during this period.

Update: Synapse is up now. Upgrade will continue on 28/10/2024 08:00 PM IST.

Update2: Synapse is upgraded to 1.85 now. Upgrade will continue on 29/10/2024 08:00 PM IST. Login is facing issues, giving "Incorrect username and/or password." error.

Update3: Debugged login issue. Upgrade will be continued on 30/10/2024 08:00 PM IST.

Update4: Updated Worker configuration, homeserver configuration and nginx configuration. Systems seems to be stable now.

Update5: Matrix will be down for 2-3 hours at 11PM IST on 20/11/2024 for synapse upgrade.
