---
title: "Poddery Bullseye update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-11-27 20:50:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-11-27 23:05:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
  - matrix
  - xmpp
  - diaspora

# Don't change the value below
section: issue
---
We have updated poddery.com server to bullseye.

