---
title: "Gitlab 16.0.8 Security update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2023-10-07 02:11:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2023-10-07 2:45:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
 - gitlab

# Don't change the value below
section: issue
---
Updated git.fosscommunity.in to v16.0.8
