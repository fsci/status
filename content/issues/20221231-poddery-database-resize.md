---
title: "Poddery Database Resizing"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2022-12-31 00:00:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2022-12-31 00:48:00
# You can use: down, disrupted, notice
severity: down
# affected sections (array). Uncomment the affected one's
affected:
  - matrix
  - xmpp
  - diaspora
# - gitlab
  - website
# - mailing-lists
# - loomio
# - videos

# Don't change the value below
section: issue
---
Poddery server maintainance from Saturday 31/12/2022 00:00 IST for database partition extension. The process may take upto 1 hour depending on the maintenance required and you may experience some downtime during this period.
