---
title: Update gitlab to the latest
date: 2021-10-05 22:29:00
resolved: true
resolvedWhen: 2020-05-05 23:45:00
# You can use: down, disrupted, notice
informational: true
severity: disrupted 
affected: 
    - gitlab    
section: issue
---
Updated Gitlab to the latest version
