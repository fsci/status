---
title: "GitLab v14.7.7 security+feature update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2022-04-02 23:45:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2022-04-03 01:44:00
# You can use: down, disrupted, notice
severity: disrupted
# affected sections (array). Uncomment the affected one's
affected:
  - gitlab
# Don't change the value below
section: issue
---

We have updated Gitlab to v14.7.7

