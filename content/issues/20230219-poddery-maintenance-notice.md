---
title: "Poddery Maintenance Notice"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2023-02-19 21:00:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2023-02-19 21:55:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
  - matrix
  - xmpp
  - diaspora
# - gitlab
# - website
# - mailing-lists
# - loomio
# - videos
# - planet
# - jitsi

# Don't change the value below
section: issue
---
Sunday 19/02/2023 21:00 IST is scheduled for updating synapse to the latest
release. The process may take up to 1 hour and you may experience some downtime
for poddery.com during this period.

UPDATE: poddery.com is now running synapse v1.77.0.
