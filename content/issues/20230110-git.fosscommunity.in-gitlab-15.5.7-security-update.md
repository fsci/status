---
title: "Gitlab 15.5.7 security update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2023-01-10 23:40:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2023-01-11 00:28:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
 - gitlab

# Don't change the value below
section: issue
---
Updated Gitlab to version 15.5.7
