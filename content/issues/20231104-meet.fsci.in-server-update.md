---
title: "meet.fsci.in Debian version update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2023-11-04 23:43:00
# Status of the issue. Boolean value: true, false
resolved: yes
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2023-11-05 00:12:00
# You can use: down, disrupted, notice
severity: disrupted
# affected sections (array). Uncomment the affected one's
affected:
# - matrix
# - xmpp
# - diaspora
# - gitlab
# - website
#  - mailing-lists
 - loomio
# - videos
# - planet
 - jitsi
 - wiki

# Don't change the value below
section: issue
---
We have upgraded the server to bookworm.
