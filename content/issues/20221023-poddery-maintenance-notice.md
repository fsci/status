---
title: "Poddery Maintenance Notice"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2022-10-24 23:00:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2022-10-31 13:30:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
  - matrix
  - xmpp
  - diaspora
# - gitlab
# - website
# - mailing-lists
# - loomio
# - videos
# - planet
# - jitsi

# Don't change the value below
section: issue
---
Poddery server has to be checked for potential dirve-failure related issues.
Expecting a downtime of 1 hour starting from 11:00 PM IST on 24/10/2022, Monday.

UPDATE [11:00 PM, 24th]: It is confirmed that we're facing serious disk errors. All poddery
services (diaspora, xmpp and matrix) will be down until a further announcement
to take care of this issue.

UPDATE [01:30 PM, 31st]: Replaced the faulty disks and all services are up and
running now. Thanks for your patience and cooperation.
