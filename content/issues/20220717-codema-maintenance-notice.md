---
title: "Codema Maintenance Notice"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2022-07-17 14:00:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2022-07-17 20:00:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
# - matrix
# - xmpp
# - diaspora
# - gitlab
# - website
# - mailing-lists
  - loomio
# - videos
# - planet
# - jitsi

# Don't change the value below
section: issue
---
Sunday 17/07/2022 14:00 IST is scheduled for testing loomio migration to new server. The process may take up to 4 hours and you may experience some downtme for codema.in during this period.

Update: The process started later than planned and ended without any downtme.
