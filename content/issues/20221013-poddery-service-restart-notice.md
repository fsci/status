---
title: "Poddery Matrix and XMPP Restart Notice"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2022-10-13 22:00:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2022-10-14 01:35:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
  - matrix
  - xmpp
# - diaspora
# - gitlab
# - website
# - mailing-lists
# - loomio
# - videos
# - planet
# - jitsi

# Don't change the value below
section: issue
---
Poddery Matrix and XMPP services will be restarted on 14/10/2022 at 10:00 PM
IST as part of regular maintenance.
