---
title: "Gitlab 13.7.8 security update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-03-06 17:30:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-03-06 19:15:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
 - gitlab

# Don't change the value below
section: issue
---
We updated gitlab to 13.7.8 feature + security release.
