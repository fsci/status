---
title: "{{ replace .Name "-" " " | title }}"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date:
# Status of the issue. Boolean value: true, false
resolved: false
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen:
# You can use: down, disrupted, notice
severity:
# affected sections (array). Uncomment the affected one's
affected:
# - matrix
# - xmpp
# - diaspora
# - gitlab
# - website
# - mailing-lists
# - loomio
# - videos
# - planet
# - jitsi
# - wiki

# Don't change the value below
section: issue
---
